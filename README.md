# open-av4ms

`Open-av4ms` is used to monitor and log serial output of a battery charger called `AV4m+/AV4ms`.
Logs and plots are accessible with your browser. Open-av4ms is mainly written in python, debian packaging is available.
Initially, av4ms was developped as a webserver baѕed charging logger for the Raspberry Pi SoC series, compatible up to pi version 3.

Open-av4ms is a fork of [Joachim & Tim Fahrners AV4MS project](http://av4ms.fahrner.name/). This repository is created with the original developers permission and forked off the latest debian package release v1.5b, 2017-08-04.

[Screenshot](http://av4ms.fahrner.name/media/img/screenshot/homescreen.png)

[Installation instructions (de)](http://av4ms.fahrner.name/index.php?page=instruction)

Hardware compatibility:
The [AV4m+ and aV4ms](http://www.logview.info/) chargers are commercially available but heavily modified variants (hardware & firmware modification) of equally commercially available battery chargers [AV4 and AV4m](http://www.mec-energietechnik.at/de/av4m).

Other loggers usable with these battery chargers are i.e.: [LogView](http://www.logview.info/) (windos only), [DataExplorer](http://www.nongnu.org/dataexplorer/download.en.html) (Java app) and Virtual-Display (features for visually impaired users, windos only).
